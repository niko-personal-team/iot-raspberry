from datetime import datetime

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
from random import random
from time import sleep

# You can generate a Token from the "Tokens Tab" in the UI
token = "Hh99w81saFJtlO9mWFiuYt39JS_CyXAqBKaVaRRH5OUWwhDM4lmITxNQf26pipooUoWUUq_Bj8wLUMM9hPG45w=="
org = "nikobonomi@gmail.com"
bucket = "Weater station"

client = InfluxDBClient(url="https://eu-central-1-1.aws.cloud2.influxdata.com", token=token)

if __name__ == "__main__":
    write_api = client.write_api(write_options=SYNCHRONOUS)

    for i in range(20):
        value = random() * 10
        data = f"test,host=arduino value={value}"
        write_api.write(bucket, org, data)
        print(f"data written {value}")
        sleep(10)
