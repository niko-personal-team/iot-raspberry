import pygatt
from binascii import hexlify
import struct
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
from random import random
from time import sleep

# You can generate a Token from the "Tokens Tab" in the UI
token = "Hh99w81saFJtlO9mWFiuYt39JS_CyXAqBKaVaRRH5OUWwhDM4lmITxNQf26pipooUoWUUq_Bj8wLUMM9hPG45w=="
org = "nikobonomi@gmail.com"
bucket = "Weater station"

DEVICE_MAC_ADDRESS = '92:7D:B3:FD:A4:EE'
TEMPERATURE_UUID = "19b10003-e8f2-537e-4f6c-d104768a1214"
LIGHT_UUID = "19b10001-e8f2-537e-4f6c-d104768a1214"
HUM_UUID = "19b10002-e8f2-537e-4f6c-d104768a1214"
PRESSURE_UUID = "19b10004-e8f2-537e-4f6c-d104768a1214"

client = InfluxDBClient(url="https://eu-central-1-1.aws.cloud2.influxdata.com", token=token)

write_api = client.write_api(write_options=SYNCHRONOUS)

temperature = 0
humidity = 0
pressure = 0
light = 0


def parse_float(raw_value):
    tuples = struct.unpack('<f', bytes(raw_value))
    return tuples[0] if len(tuples) else None


def parse_int(raw_value):
    value = int.from_bytes(bytes(raw_value), byteorder='little', signed=False)
    return value


def on_temperature_update(handle, data):
    value = round(parse_float(data), 2)
    print(f"Temperature {value} degrees celsius")
    global temperature
    temperature = value


def on_hum_update(handle, data):
    value = round(parse_float(data), 2)
    print(f"Humidity {value} %")
    global humidity
    humidity = value


def on_pressure_update(handle, data):
    value = round(parse_float(data), 2)
    print(f"Pressure {value} kPa")
    global pressure
    pressure = value


def on_light_update(handle, data):
    value = parse_int(data)
    print(f"Room light {value}")
    global light
    light = value


def write_value(value, sensor):
    try:
        data = f"{sensor},host=arduino_niko_office value={value}"
        write_api.write(bucket, org, data)
    except Exception:
        print("error on publishing " + sensor)


if __name__ == "__main__":
    ble_adapter = pygatt.GATTToolBackend()
    try:
        print("Starting the BLE device...")
        ble_adapter.start()
        ble_sense = ble_adapter.connect(DEVICE_MAC_ADDRESS)
        print("Device connected")
        ble_sense.subscribe(
            TEMPERATURE_UUID,
            callback=on_temperature_update,
            wait_for_response=False
        )
        ble_sense.subscribe(
            LIGHT_UUID,
            callback=on_light_update,
            wait_for_response=False
        )
        ble_sense.subscribe(
            HUM_UUID,
            callback=on_hum_update,
            wait_for_response=False
        )
        ble_sense.subscribe(
            PRESSURE_UUID,
            callback=on_pressure_update,
            wait_for_response=False
        )

        sleep(10)

        while (True):
            write_value(temperature, "temperature")
            sleep(7)
            write_value(humidity, "humidity")
            sleep(7)
            write_value(pressure, "pressure")
            sleep(7)
            write_value(light, "light")
            sleep(7)

    finally:
        ble_adapter.stop()
